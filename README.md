Prácticas de laboratorio de Estructura de Computadores
======================================================

Diseño de prácticas de laboratorio para la asignatura "Estructura de Computadores"
de las titulaciones de Ingeniería Informática de la ETSI Informática de Sevilla.

El material en este repositorio es un espacio de trabajo del autor y no tiene
por qué coincidir con prácticas impartidas oficialmente en las asignaturas.

Listado de prácticas
--------------------

* [Gate Control](gate_control/): Control de una barrera (Verilog).

* [Simple RPN Calculator](simple_rpn_calculator/): Calculadora de notación
  polaca inversa (RPN) simple (Verilog).

* [AVR Blink](avr_blink): Introducción a la programación en ensamblador de
  microcontroladores AVR (para placas Arduino UNO).

* [AVR Dice](avr_dice): Dado electrónico en ensamblador para AVR.

Autores
-------

Jorge Juan-Chico <jjchico@dte.us.es> y otros (consultar documentos).

Licencia
--------

CC BY-SA 4.0 [Creative Commons Atribution-ShareAlike 4.0 International] (https://creativecommons.org/licenses/by-sa/4.0/)
