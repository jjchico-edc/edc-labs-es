// Design:      Simple RPN calculator
// File:        control_unit_tb.v
// Description: Control unit. Test bench.
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-28 (initial)

`timescale 1ns / 1ps

module test ();

    reg clk;
    reg reset;
    reg enter;
    reg add;
    reg sub;
    reg pm;
    wire [1:0] op;       // ALU control
    wire y_w;             // yreg write
    wire s_w;              // status reg write

    // Control unit instance
    control_unit control_unit (
        .clk(clk),
        .reset(reset),
        .enter(enter),
        .add(add),
        .sub(sub),
        .pm(pm),
        .op(op),       // ALU control
        .y_w(y_w),             // yreg write
        .s_w(s_w)              // status reg write
    );

    // Clock generator (T=20ns, f=50MHz)
    always
        #10 clk <= ~clk;

    // Main simulation process

    // This process tells the control unit to:
    // - enter a number
    // - change the sign of the entered number
    // - do an "add" operation
    // - enter another number
    // - do a substract operation

    initial begin
        // Signal initialization
        reset = 1'b1;
        clk = 1'b0;
        {enter, add, sub, pm} = 4'b0000;

        // Waveform generation (optional)
        $dumpfile("control_unit_tb.vcd");
        $dumpvars(0, test);

        // Output printing
        $display("reset enter add sub  pm : op y_w s_w");
        $monitor("    %b    %b   %b   %b   %b :  %d   %b   %b",
                 reset, enter, add, sub, pm, op, y_w, s_w);

        // Operation
        @(posedge clk) #1;              // wait one clock cycle and do
        reset = 1'b0;                   // reset
        @(posedge clk) #1;
        reset = 1'b1;
        repeat(3) @(posedge clk) #1;

        enter = 1'b1;                   // press "enter"
        repeat(3) @(posedge clk) #1;
        enter = 1'b0;
        repeat(3) @(posedge clk) #1;

        pm = 1'b1;                      // press "pm"
        repeat(3) @(posedge clk) #1;
        pm = 1'b0;
        repeat(3) @(posedge clk) #1;

        add = 1'b1;                     // press "add"
        repeat(3) @(posedge clk) #1;
        add = 1'b0;
        repeat(3) @(posedge clk) #1;

        enter = 1'b1;                   // press "enter"
        repeat(3) @(posedge clk) #1;
        enter = 1'b0;
        repeat(3) @(posedge clk) #1;

        sub = 1'b1;                     // press "sub"
        repeat(3) @(posedge clk) #1;
        sub = 1'b0;
        repeat(3) @(posedge clk) #1;

        $finish;                        // finish the simulation
    end
endmodule