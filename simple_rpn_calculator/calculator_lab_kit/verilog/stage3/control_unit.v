// Design:      Simple RPN calculator
// File:        control_unit.v
// Description: Control Unit
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-28 (initial)

module control_unit (
    input wire clk,         // clock (rising edge)
    input wire reset,       // reset (synchronous, active-low)
    input wire enter,       // enter key
    input wire add,         // add key
    input wire sub,         // subtract key
    input wire pm,          // plus/minus key

    output reg [1:0] op,   // ALU operation control
    output reg y_w,        // yreg (accumulator) write control
    output reg s_w         // sreg (status) write control
);

    /* -------------------------------------- */
    
      // State definition
    parameter        READY = 0,
                     WAIT  = 1;

    // State variables
    reg [1:0] state, next_state;

    // State change process
    always @(posedge clk, negedge reset)
        if (reset == 1'b0)
            state <= READY;
        else
            state <= next_state;
            
            //complete
    
endmodule
