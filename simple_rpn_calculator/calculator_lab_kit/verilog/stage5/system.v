// Design:      Simple RPN calculator
// File:        system.v
// Description: Calculator system for development board implementation.
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-03-01 (initial)

module system (
    input wire clk,             // clock (rising edge)
    // input wire reset,           // reset (synchronous, active-low)
    input wire enter,           // enter key
    input wire add,             // add key
    input wire sub,             // subtract key
    input wire pm,              // plus/minus key
    input wire [7:0] data_in,      // data input
    output wire f_c,            // flag carry
    output wire f_v,            // flag overflow
    output wire [3:0] an,       // 7-segment anode control
    output wire [0:6] seg,      // 7-segment code (active-low)
    output wire dp              // 7-segment decimal point output
);

    /* --------------------------------------- */
    
    
    //escribir instancia de la calculadora
    
    
      // 7-segment controller instance
    display_ctrl #(.cdbits(18), .hex(1)) display_ctrl (
        .ck(clk),              // system clock
        .x3(data_in[7:4]),        // display digits, from left to right
        .x2(data_in[3:0]),
        .x1(yreg[7:4]),
        .x0(yreg[3:0]),
        .dp_in(4'b1011),     // decimal point vector
        .seg(seg),       // 7-segment output
        .an(an),        // anode output
        .dp(dp)               // decimal point output
    );

endmodule
