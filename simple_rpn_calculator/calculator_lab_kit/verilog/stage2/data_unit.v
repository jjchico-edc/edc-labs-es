// Design:      Simple RPN calculator
// File:        data_unit.v
// Description: Data Unit
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-27 (initial)

module data_unit #(
    parameter W = 8     // Data width
)(
    input wire clk,
    input wire [W-1:0] data_in,
    input wire [1:0] op,
    input wire y_w,
    input wire s_w,
    output reg [W-1:0] yreg,       // Y register
    output wire f_c,             // flag carry
    output wire f_v              // flag overflow
);

    /* ---------------------------------- */

/* Se debe instaciar la alu descrita en la fase 1*/


    wire [W-1:0] alu_out;
    wire c, v;
    reg [1:0] sreg;         // Status register

    // ALU instance completar
    
// Y register (accumulator)
    always @(posedge clk)
        if (y_w)
            yreg <= alu_out;

    // Status register
    always @(posedge clk)
        if (s_w)
            sreg <= {v, c};

    // Status flags output
    assign f_c = sreg[0];
    assign f_v = sreg[1];
endmodule


endmodule
