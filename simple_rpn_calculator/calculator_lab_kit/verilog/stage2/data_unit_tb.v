// Design:      Simple RPN calculator
// File:        data_unit_tb.v
// Description: Data unit. Test bench.
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-27 (initial)

`timescale 1ns / 1ps

module test ();

    reg clk;
    reg [7:0] x_in;
    reg [1:0] op;
    reg y_w;
    reg s_w;
    wire [7:0] yreg;       // Y register
    wire f_c;             // flag carry
    wire f_v;              // flag overflow

    // Data unit instance
    data_unit #(.W(8)) data_unit (
        .clk(clk),
        .x_in(x_in),
        .op(op),
        .y_w(y_w),
        .s_w(s_w),
        .yreg(yreg),       // Y register
        .f_c(f_c),             // flag carry
        .f_v(f_v)              // flag overflow
    );

    // Clock generator (T=20ns, f=50MHz)
    always
        #10 clk <= ~clk;

    // Main simulation process

    // This process emulates the control unit.
    // We make the data unit do: Y = 7 - 3.
    // Operation (active control signals)
    // x_in = 7; yreg <- x_in                         (op=TRA, y_w)
    // x_in = 3; yreg <- yreg - x_in; sreg <- {v,c}   (op=SUB; y_w)

    initial begin
        // Signal initialization
        clk = 1'b0;
        x_in = 0;
        op = 0;     // ALU_TRA
        y_w = 0;
        s_w = 0;

        // Waveform generation (optional)
        $dumpfile("data_unit_tb.vcd");
        $dumpvars(0, test);

        // Output printing

        // Operation
        repeat(3) @(posedge clk);   // wait 3 clock cycles
        #1;                         // allow some time before changing
        x_in = 7;                   // the inputs (hold time)
        op = 2;
        y_w = 1'b1;
        @(posedge clk);             // wait for next clock edge
        #1;
        if (yreg != 7)              // expect: yreg=7
            $display("ERROR: yreg=%d, expected 7.", yreg);
        x_in = 3;                   // setup next input values
        op = 1;
        s_w = 1'b1;
        @(posedge clk);
        #1;
        if (yreg!=4 || f_c!=1'b1 || f_v!=1'b0) // expect: yreg=4, c=1, v=0
            $display("ERROR: yreg=%d, f_c=%b, f_v=%b, expected 4, 1, 0.",
                     yreg, f_c, f_v);
        y_w = 1'b0;
        s_w = 1'b0;
        repeat(2) @(posedge clk);
        #1;                         // y reg should still be 4
        x_in = 125;                 // Do yreg <- yreg + 125
        op = 0;
        y_w = 1'b1;
        s_w = 1'b1;
        @(posedge clk);
        #1;
        if (yreg!=129 || f_c!=1'b0 || f_v!=1'b1) // expect: yreg=129, c=0, v=1
            $display("ERROR: yreg=%d, f_c=%b, f_v=%b, expected 129, 0, 1.",
                     yreg, f_c, f_v);
        y_w = 1'b0;
        s_w = 1'b0;
        repeat(2) @(posedge clk);

        $finish;                    // finish the simulation
    end
endmodule