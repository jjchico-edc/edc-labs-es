// Design:      Simple RPN calculator
// File:        calculator_tb.v
// Description: Complete calculator test bench.
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-03-01 (initial)

`timescale 1ns / 1ps

module test ();

    reg clk;
    reg reset;
    reg enter;
    reg add;
    reg sub;
    reg pm;
    reg [7:0] x_in;
    wire [7:0] yreg;
    wire f_c;
    wire f_v;

    // Calculator instance
    calculator #(.W(8)) calculator (
        .clk(clk),
        .reset(reset),
        .enter(enter),
        .add(add),
        .sub(sub),
        .pm(pm),
        .x_in(x_in),
        .yreg(yreg),
        .f_c(f_c),
        .f_v(f_v)
    );

    // Clock generator (T=20ns, f=50MHz)
    always
        #10 clk <= ~clk;

    // Main simulation process

    // This process tells the calculator to:
    // - enter number 7
    // - change the sign of the entered number
    // - enter number 20 and add
    // - enter number 5 and substract

    initial begin
        // Signal initialization
        reset = 1'b1;
        clk = 1'b0;
        {enter, add, sub, pm} = 4'b0000;
        x_in = 0;

        // Waveform generation (optional)
        $dumpfile("calculator_tb.vcd");
        $dumpvars(0, test);

        // Output printing
        $display("reset enter add sub  pm : x_in yreg f_c f_v");
        $monitor("    %b     %b   %b   %b   %b :  %d  %d   %b   %b",
                 reset, enter, add, sub, pm, x_in, yreg, f_c, f_v);

        // Operation
        @(posedge clk) #1;              // wait one clock cycle and do
        reset = 1'b0;                   // reset
        @(posedge clk) #1;
        reset = 1'b1;
        repeat(3) @(posedge clk) #1;

        x_in = 7;                       // Y <- 7
        enter = 1'b1;                   // press "enter"
        repeat(3) @(posedge clk) #1;
        enter = 1'b0;
        repeat(3) @(posedge clk) #1;

        pm = 1'b1;                      // Y <- -Y
        repeat(3) @(posedge clk) #1;
        pm = 1'b0;
        repeat(3) @(posedge clk) #1;

        x_in = 20;                      // Y <- Y + 20
        add = 1'b1;
        repeat(3) @(posedge clk) #1;
        add = 1'b0;
        repeat(3) @(posedge clk) #1;

        x_in = 5;                       // Y <- Y - 5
        sub = 1'b1;
        repeat(3) @(posedge clk) #1;
        sub = 1'b0;
        repeat(3) @(posedge clk) #1;

        $finish;                        // finish the simulation
    end
endmodule