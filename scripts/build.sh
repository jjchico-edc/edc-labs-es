#!/bin/sh -e

paths="gate_control simple_rpn_calculator avr_blink"
mkdir -p build

for dir in $paths; do
    cd "$dir"
    if ls *.odt; then
        lowriter --convert-to pdf --outdir ../build *.odt
    fi
    if [ -d *lab_kit ]; then
        zip -r "../build/${dir}_lab_kit.zip" *lab_kit
    fi
    cd ..
done
