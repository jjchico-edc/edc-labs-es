// Módulo: system - banco de pruebas
// Descripción: Banco de pruebas del sistema
// Proyecto: Sistema de control de una barrera
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Fecha original: 21-02-2019

`timescale 1ns / 1ps

module test();

    localparam SYS_FREQ = 100;            // frecuencia del sistema (Hz)
    localparam CYCLE = 10**9/SYS_FREQ;    // periodo (ns)

    reg clk;        // reloj del sistema
    reg reset;      // puesta a cero del sistema
    reg open;       // señal de apertura (activa en alto)
    wire gate;      // salida de control (0-cerrar, 1-abrir)
    wire servo;     // salida de control del servo

    // diseño bajo test
    system #(
        .SYS_FREQ(SYS_FREQ)
        ) dut (
        .clk(clk),
        .reset(reset),
        .open(open),
        .servo(servo),
        .gate(gate)
        );

    // generador de reloj
    always
        #(CYCLE/2) clk = ~clk;

    initial begin
        clk = 0;
        reset = 0;
        open = 0;

        // archivo de formas de onda
        $dumpfile("system_tb.vcd");
        // guardamos todas las señales en "test"
        $dumpvars(0, test);

        // cambios de entradas
        repeat(1*SYS_FREQ) @(negedge clk);      // esperamos 1s
        reset = 1;                              // puesta a cero
        repeat(1*SYS_FREQ) @(negedge clk);
        reset = 0;
        repeat(1*SYS_FREQ) @(negedge clk);      // esperamos 1s
        open = 1;                               // pulsamos el botón
        repeat(1*SYS_FREQ) @(negedge clk);
        open = 0;
        repeat(3*SYS_FREQ) @(negedge clk);      // esperamos 3s
        if(gate != 1'b1)
            $display("ERROR: %t: gate=%b, esperaba 1.", $time, gate);
        repeat(10*SYS_FREQ) @(negedge clk);     // esperamos 10s
        if(gate != 1'b0)
            $display("ERROR: %t: gate=%b, esperaba 0.", $time, gate);
        repeat(1*SYS_FREQ) @(negedge clk);      // esperamos 1s
        open = 1;                               // pulsamos el botón
        repeat(1*SYS_FREQ) @(negedge clk);
        open = 0;
        repeat(3*SYS_FREQ) @(negedge clk);      // esperamos 3s
        open = 1;                               // pulsamos el botón
        repeat(1*SYS_FREQ) @(negedge clk);
        open = 0;
        repeat(12*SYS_FREQ) @(negedge clk);     // esperamos 12s
        if(gate != 1'b0)
            $display("ERROR: %t: gate=%b, esperaba 0.", $time, gate);

        $finish;
    end
endmodule
