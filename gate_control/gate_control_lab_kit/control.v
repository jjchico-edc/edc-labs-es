// Módulo: control
// Descripción: Control de barrera con temporizador. Unidad de control.
// Proyecto: Sistema de control de una barrera
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Fecha original: 21-02-2019

module control (
    input wire clk,         // reloj del sistema
    input wire reset,       // puesta a cero del sistema
    input wire open,        // señal de apertura (activa en alto)
    input wire eoc,         // fin de cuenta del temporizador
    output reg clear,       // puesta a cero del contador
    output reg enable,      // habilitación del temporizador
    output reg gate         // salida de control (0-cerrar, 1-abrir)
    );

    // estados
    localparam CLOSED  = 2'd0,
               OPEN    = 2'd1;

               /*** añade más estados si es necesario ***/

    // variables de estado y próximo estado
    reg [1:0] state = CLOSED;   // estado inicial = CLOSED
    reg [1:0] next_state;

    // proceso de cambio de estado

    /*** añade tu código ***/

    // proceso de cálculo de próximo estado y salida

    /*** añade tu código ***/

endmodule // control
