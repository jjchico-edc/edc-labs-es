// Módulo: timer
// Descripción: Temporizador
// Proyecto: Sistema de control de una barrera
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Fecha original: 21-02-2019

// Contador/temporizador

/*
   necesitamos un contador que pueda ser habilitado y deshabilitado,
   puesto a cero y que genere una señal de fin de cuenta cuando alcance
   un valor máximo.
 */

module timer #(
    parameter SYS_FREQ = 50000000,  // frecuencia de reloj del sistema (Hz)
    parameter DELAY = 10            // tiempo de expiración (s)
    )(
    input wire clk,         // reloj del sistema
    input wire clear,       // puesta a cero del sistema
    input wire enable,      // habilitación
    output wire eoc         // fin de cuenta
    );

    // valor del contador para generar la señal de fin de cuenta
    localparam MAX_COUNT = SYS_FREQ * DELAY;

    // estado y señales del contador
    /* debe tener suficientes bits para almacenar MAX_COUNT */
    reg [/*** ***/] count = 0;      // estado

    // descripción del contador

    /*** código del contador ***/

    /*** generación fin de cuenta ***/

endmodule // timer
